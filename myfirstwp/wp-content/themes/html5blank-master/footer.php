			<!-- footer -->
			<footer class="footer" role="contentinfo">
			<div class="footer-widget">
				<?php if(!function_exists('dynamic_footer') || !dynamic_footer('widget-area-2')) ?>
			</div>
				<!-- copyright -->
				<p class="copyright">
					<p>Designed & Developed by <?php bloginfo('name'); ?>
					&copy; <?php echo date('Y'); ?></p>
					<!-- <a href="//wordpress.org" title="WordPress">WordPress</a> &amp; <a href="//html5blank.com" title="HTML5 Blank">HTML5 Blank</a>. -->
				</p>
				<!-- /copyright -->
			</footer>
			<!-- /footer -->

		</div>
		<!-- /container -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script src="https://rawgithub.com/kswedberg/jquery-smooth-scroll/master/jquery.smooth-scroll.min.js"></script>
		<script>
		// 	$(function(){
		// 		console.log("What up?");
		// 		$('.nav a').smoothScroll(){
		// 			offset: 40px,
		// 			speed: 5000
		// 		});
		// 	});
		// </script>	
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
