 <?php 
/*
Template Name: Jonny's Homepage 
*/
get_header(); ?>

	<main role="container clearfix">
		<!-- section -->
		<!-- start of about section/wrapper -->
		<section class="about clearfix" id="about">
			<div class="wrapper">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<img class="bioPic" src="<?php echo get_template_directory_uri(); ?>/img/jonny-james.jpg" alt="jonny james">
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
				<div class="hackeryou">
					<ul>
					<li><i class="fa fa-sliders fa-5x"></i>
					<p>Keen interest in Public Health and a strong desire to improve lives through UX/UI</p></li>
					<li><i class="fa fa-tencent-weibo fa-5x"></i>
					<p>HTML5, CSS, JavaScript, jQuery, Responsive Design and WordPress</p></li>
					<li><i class="fa fa-signal fa-5x"></i>
					<p>Learning AngularJS and nodeJS while honing my skills in Design and Accessibility</p></li>
					</ul>
				</div>
			</div>
		</section>
		<!-- this ends the about wrapper -->
		<!-- this is the start of the portfolio section/wrapper -->
		<section class="portfolio clearfix" id="portfolio">
			<div class="wrapper">
			<h1>Portfolio</h1>
				<?php $portfolioItems = new WP_Query(
					array(
						'posts_per_page' => 3,
						'post_type' => 'portfolio'
						)		
					);?>	
				<?php if ($portfolioItems->have_posts() ) : ?>	
				<?php while ($portfolioItems->have_posts()) : $portfolioItems->the_post(); ?>
				<div class="portfolioItem">
					<h1><?php the_title() ?></h1>
					<?php the_content() ?>
					<?php the_post_thumbnail( 'square' ); ?>
				</div>
				<?php endwhile; endif; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</section>
			<div class= "facts clearfix">
				<h1>Facts of Life</h1>
					<div class="fact1">
						<img src="<?php echo get_template_directory_uri(); ?>/img/johnnycash.jpg" alt="johnny cash"><p class="factone">My First Concert: "The Man In Black"</p>
					</div>
					<div class="fact2">
						<img src="<?php echo get_template_directory_uri(); ?>/img/2014boston.jpg" alt="Boston 2014"><p class="facttwo">Ran the Boston Marathon this Year</p>
					</div>
					<div class="fact3">
						<img src="<?php echo get_template_directory_uri(); ?>/img/seven.jpg" alt="Lucky 7"><p class="factthree">My Birthday contains the maximum possible number of 7's</p>	
					</div>	
			</div>
			<!-- </div>	 -->
			<?php //$facts = new WP_Query( 'page_id=135' ); ?>
			<?php //if ( $facts->have_posts() ) : ?>	
				<?php //while ($facts->have_posts()) : $facts->the_post(); ?>
			<!-- <div class="facts"> -->
				<h1><?php //the_title() ?></h1>
				<?php //the_content() ?>
			</div>
			<?php //endwhile; endif; ?>
		</div>
		<!-- <section class="facts" id="facts">
			<div class="wrapper">
			</div>
		</section>		 -->
			<!-- this ends the portfolio wrapper -->
			<!-- start of the blog section -->
		<!-- <section class="blog" id="blog">
			<div class="wrapper">
				<//?php $blogItems = new WP_Query( 
					array( 
						'posts_per_page' => 3, 
						'post_type' => 'post' 
						) 
					); ?>
				<//?php if ( $blogItems ->have_posts() ): ?>
				<//?php while ( $blogItems->have_posts()) : $blogItems->the_post(); ?>
				<div class="blogItem">	
					<h1><//?php the_title(); ?></h1>
					<//?php the_content(); ?>
				</div>	

				<//?php endwhile; endif ?>
			</div>
		</section> -->
			<!-- end of the blog section -->
		<section class="contactInfo clearfix" id="contact">
			<h1>Let's Connect</h1>
			<p class="emailLink">
			<a class="emailLink" href="mailto:doublej@jonnyjames.com">doublej@jonnyjames.com</a>
			</p>
		<div class="social">
			<ul>
			 	<li><a href="http://www.twitter.com/ramblapacifico" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a></li>
			 	<li><a href="http://www.github.com/jonnyjames" target="_blank"><i class="fa fa-github-square fa-2x"></i></a></li>
			 	<li><a href="http://ca.linkedin.com/in/1jonnyjames/" target="_blank"><i class="fa fa-linkedin-square fa-2x"></i></li></a>
			 	<li><a href="http://www.ramblapacifico.tumblr.com" target="_blank"><i class="fa fa-tumblr-square fa-2x"></i></a></li>
			 </ul>	
		</div>	
		</section>	
			<!-- section -->
	</main>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>
