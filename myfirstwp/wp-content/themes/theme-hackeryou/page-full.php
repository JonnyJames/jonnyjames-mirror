<?php

/*
	Template Name: Full Page, No Sidebar
*/ 

get_header();  ?>

<div class="section">
  <div class="innerWrapper">
  Bacon ipsum dolor sit amet drumstick pastrami ball tip tongue, turducken pancetta shankle pork belly prosciutto jerky fatback tail beef. Kevin rump flank, drumstick porchetta pork loin hamburger capicola jowl short loin pastrami. Tail strip steak tri-tip ribeye brisket pork belly venison rump shoulder. Meatball landjaeger rump doner short loin chicken porchetta pig andouille tongue t-bone brisket. Tenderloin ham hock meatloaf hamburger pastrami tri-tip pig tail short ribs chuck fatback beef ribs venison t-bone. Turkey andouille jerky leberkas beef capicola landjaeger chuck flank frankfurter tri-tip shoulder biltong. Tongue t-bone boudin, ball tip kevin drumstick ground round bresaola chuck brisket short ribs ham ribeye shank.


    <div class="full">
      <?php // Start the loop ?>
      <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>


      <?php endwhile; // end the loop?>
    </div>
  </div> <!-- /.innerWrapper -->
</div> <!-- /.section -->
<?php get_footer(); ?>