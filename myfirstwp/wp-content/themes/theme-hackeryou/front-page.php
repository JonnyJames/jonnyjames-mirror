<?php
get_header();  ?>

<div class="section">
  <div class="innerWrapper">



    <div class="full">

      <?php $allPages = new WP_Query (
        array(
          'posts_per_page' => -1,
          'post_type' => 'page',
          'order' => 'ASC',
          'post__not_in' => array(14)
          )
      ); ?>
      
      <?php // Start the loop ?>
      <?php if ( $allPages->have_posts() ) while ( $allPages->have_posts() ) : $allPages->the_post(); ?>

        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>


      <?php endwhile; // end the loop?>
    </div>
  </div> <!-- /.innerWrapper -->
</div> <!-- /.section -->
<?php get_footer(); ?>