<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jonnyjam_portfolio');

/** MySQL database username */
define('DB_USER', 'doublej');

/** MySQL database password */
define('DB_PASSWORD', 'Miss0ula');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'DJc* 0X(.+-nBlST{4-Me]NCf]~}pB9E_2:067&ny/jHvkE5y3,[T%RUmFbvgt,Z');
define('SECURE_AUTH_KEY',  '_6+@,S>S0Nr+hLl]4X z@Ooch9vdE |tb3~$qh8.o&yY+c`.X[t[nDO56 m4 f<P');
define('LOGGED_IN_KEY',    'Kxkp-34XR7gp}JO:k]0(jPRCTb m71![n3H~DW4Vh]Wq<>S;o*)SMTPt=]M~zh:$');
define('NONCE_KEY',        ';)z-3(aU:Xhgze5-RtL(EwKl-|&<s6A{f-$&AKh!DJ[Q.VyR-YuF}[flp0;`?X<O');
define('AUTH_SALT',        'WU3!Or!1myHWL(MnWCzNfiI5zX4,H~9Ax30-2!cT7b/R[g.|3P/wCx!.&+n1>jxP');
define('SECURE_AUTH_SALT', 'XI;LI_TokaoPjmh*o3@m~71(I-*Wr%kX!xL[xx.-gv2/vcnv?hfl:|*,FI& i9 7');
define('LOGGED_IN_SALT',   ':dHI!8!=a_zMj#wzPxGeN+_xX&bW^&5DV~SPGZ|1X`5,6+f!KB]GPD+TUbF0:EZ?');
define('NONCE_SALT',       'AY{SNohdS&UTeL$9XN(7l42v/mo4/!rwlQ@nk|yn-u^soSzqjk>Q%K9^~:/@<N&S');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
// Make this "true" when you are working on the site, but change back to "false" when it is being published.
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
